

## Test Case ID 031
## Test Case Name
Verify the promoter section in the Audience section
## Pre-conditions 
Make sure the user is signed in Demio
Make sure there is a created event with the sessions


## Steps To Reproduce

1. Click on the created event
2. Click on the drop-down near the button Join Room
3. Select "View Registration Page"
4. Fill the credentials and click on the button "Register"
5. Click on the Boost registration button
6. Click on the name of the created event
7. Check the name and email of the registrant below the Promoter section
## Expected Result 

The name and  email of the registrant is displayed below the Promoter section

----------------------------------------------------------------------------------
## Test Case ID 032
## Test Case Name
Verify the promoter section in the Audience section
## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with the sessions


## Steps To Reproduce

1. Click on the created event
2. Click on the drop-down near the button Join Room
3. Select "View Registration Page"
4. Fill the credentials and click on the button "Register"
5. Click on the Boost registration button
6. Click on the name of the created event
7. Check the name and email of the registrant below the Promoter section
## Expected Result 
The name and  email of the registrant is displayed below the Promoter section

----------------------------------------------------------------------------------

## Test Case ID 032
## Test Case Name
Verify the sessions  in the Audience section
## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with session
## Steps To Reproduce

1. Click on the created event
2. Click on the button "Schedule sessions".
3. Select date and time of the session
4. Add some sessions
5. Click on the drop-down near the button Join Room  
6. Select "View Registration Page"
7. Select upcoming session
8. Fill the credentials and click on the button "Register"
9. Click on the Boost registration button
10. Click on the name of the created event 
11. Check the date and time of the session below the section "Sessions"
## Expected Result 
The date and time of the session  according to registrant is displayed below the section "Sessions" 

-----------------------------------------------------------------------------------------
## Test Case ID 033
## Test Case Name
Verify the sync date  in the Audience section
## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with session
## Steps To Reproduce

1. Click on the created event
2. Click on the button "Schedule sessions".
3. Select date and time of the session
4. Add some sessions
5. Click on the drop-down near the button Join Room  
6. Select "View Registration Page"
7. Select upcoming session
8. Fill the credentials and click on the button "Register"
9. Click on the Boost registration button
10. Click on the name of the created event 
11. Check the date and time below the section "Sync Date"
## Expected Result

The sync date shows the time when the participant is registered on the Event

-----------------------------------------------------------------------------------------
## Test Case ID 034
## Test Case Name
Verify the number of the registrations in  the Audience section
## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with sessions and registrants
## Steps To Reproduce

1. Click on the Boost registration button
2. Click on the name of the created event 
3. Click on the  "Launch" button
4. Open the email which is displayed under the section "Promoter"
5. Click on the mail from  events@boost.banzai.io
6. Click on the unique sharing link
7. Share the event in FB(or on another social media)
8. Register new users by sharing the link
9. Check the number of registrations  in  the "Audience" section

## Expected Result
The number of registration is  increasing according to the number of registrants of the sharing
link
---------------------------------------------------------------------------------
## Test Case ID 035
## Test Case Name
Verify functionality of the "Re-send Email" button
## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with sessions and registrants
## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Click on the "Launch" button
4. Click on the dotes near the Sync Date
5. Select "Re-send email" option from drop-down
6. Check the email which is displayed in the Promoter's section

## Expected Result

The registrant should receive a mail again.

---------------------------------------------------------------------------------
## Test Case ID 036
## Test Case Name
Verify that the "Re-send Email" button is not visible when the event is not launched

## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with sessions and registrants
Make sure the event is not launched
## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Click on the dotes near the Sync Date
4. Check  "Re-send email" option from drop-down

## Expected Result

 "Re-send email" option from drop-down is not displayed

-----------------------------------------------------------------------------
## Test Case ID 037
## Test Case Name
Verify that the "Re-send Email" button is not visible when the event is not launched

## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with sessions and registrants
Make sure the event is not launched
## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Click on the dotes near the Sync Date
4. Check  "Re-send email" option from drop-down

## Expected Result

 "Re-send email" option from drop-down is not displayed

------------------------------------------------------------------------------
## Test Case ID 038
## Test Case Name
Verify the functionality of the button "Copy sharing link'

## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with sessions and registrants

## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Click on the dotes near the Sync Date
4. Select "Copy sharing link" option from drop-down
5. Paste the URL you just copied into the address bar of the browser
## Expected Result

 The share page should be opened
 
---------------------------------------------------------------------------------------
## Test Case ID 039
## Test Case Name
Verify the functionality of the button "Copy sharing link'

## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with sessions and registrants

## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Click on the dotes near the Sync Date
4. Select "Copy sharing link" option from drop-down
5. Paste the URL you just copied into the address bar of the browser
## Expected Result

 The share page should be opened
 
---------------------------------------------------------------------------------------
# Test Case ID 040
## Test Case Name
Verify the functionality of the button "Remove from Campaign'

## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with sessions and registrants

## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Click on the dotes near the Sync Date
4. Select "Remove from Campaign" option from drop-down

## Expected Result
 The information of the removed registrant is disabled

---------------------------------------------------------------------------------------
# Test Case ID 041
## Test Case Name
Verify the functionality of the button "Add to Campaign'

## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with sessions and registrants
Make sure the registrant is removed from Campaign

## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Find the information of the registrant which was removed
4. Click on the dotes near the Sync Date
5. Select the option "Add to campaign"
6. Click on the Confirm  button  when the pop-up is opened


## Expected Result
  The information of the registrant is enabled an added to the Campaign

  ------------------------------------------------------------------------------------
# Test Case ID 041
## Test Case Name
Verify the functionality of the button "Add to Campaign'

## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with sessions and registrants
Make sure the registrant is removed from Campaign

## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Find the information of the registrant which was removed
4. Click on the dotes near the Sync Date
5. Select the option "Add to campaign"
6. Click on the Confirm  button  when the pop-up is opened


## Expected Result
  The information of the registrant is enabled an added to the Campaign

  ------------------------------------------------------------------------------------
# Test Case ID 041
## Test Case Name
Verify the functionality of the button "Add to Campaign'

## Pre-conditions 
Make sure the user  is signed in Demio
Make sure there is a created event with sessions and registrants
Make sure the registrant is removed from Campaign

## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Find the information of the registrant which was removed
4. Click on the dotes near the Sync Date
5. Select the option "Add to campaign"
6. Click on the Confirm  button  when the pop-up is opened

## Expected Result
  The information of the registrant is enabled an added to the Campaign

  ------------------------------------------------------------------------------------
# Test Case ID 042
## Test Case Name
Verify the  placeholder text added on search bar

## Pre-conditions 
Make sure the user  is signed in  Demio
Make sure there is a created event with sessions and registrants

## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Check the placeholder  the on the search bar

## Expected Result
 The placeholder text is visible on the search bar

  ------------------------------------------------------------------------------------
# Test Case ID 043
## Test Case Name
Verify search icon is present on the field.

## Pre-conditions 
Make sure the user  is signed in  Demio
Make sure there is a created event with sessions and registrants

## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Check the search icon
## Expected Result
 The search icon is present on the field.
 -------------------------------------------------------------------------------------
## Test Case ID 044
## Test Case Name
Verify cursor should present on click on the search icon.

## Pre-conditions 
Make sure the user  is signed in  Demio
Make sure there is a created event with sessions and registrants

## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Check the cursor on the search icon
## Expected Result

 The cursor presents on click on the search icon

 -------------------------------------------------------------------------------------
## Test Case ID 045
## Test Case Name
Verify the search results are correct

## Pre-conditions 
Make sure the user  is signed in  Demio
Make sure there is a created event with sessions and registrants

## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Add the name, email or the part of the name or email in search box
## Expected Result
Search results are displayed according the input in the search box

 -------------------------------------------------------------------------------------
## Test Case ID 045
## Test Case Name
Verify the search results when the input of the user does not exist

## Pre-conditions 
Make sure the user  is signed in  Demio
Make sure there is a created event with sessions and registrants

## Steps To Reproduce
1. Click on the Boost registration button
2. Click on the name of the created event 
3. Input the name or the email which does not exist
## Expected Result
Search results are not displayed

----------------------------------------------------------------------------------------

